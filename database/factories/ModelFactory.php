<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(App\Models\Density::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'value' => $faker->randomFloat(3, 0, 10000),
        'country' => $faker->country,
        'food_category_id' => function () {
            return factory(App\Models\FoodCategory::class)->create()->id;
        }
    ];
});

$factory->define(App\Models\FoodCategory::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->text(50),
    ];
});
