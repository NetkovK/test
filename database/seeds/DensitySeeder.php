<?php

use Illuminate\Database\Seeder;

class DensitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Density::class, 50)->create();
    }
}
