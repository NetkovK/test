<?php

namespace App\Http\Controllers;

use App\Models\Density;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DensityController extends Controller
{
    /**
     * List of densities.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return response()->json(Density::with('foodCategory')->get());
    }

    /**
     * Create a density.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $density = new Density();
        $density->fill($request->all());
        $density->save();

        return response()->json($density, 201);
    }

    /**
     * Update a  density.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validateRequest($request);

        $density = Density::find($id);

        if (empty($density)) {
            return response()->json([], 404);
        }

        $density->fill($request->all());
        $density->save();

        return response()->json($density, 204);
    }

    /**
     * Delete a density.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $density = Density::find($id);

        if (empty($density)) {
            return response()->json([], 404);
        }

        $density->delete();

        return response()->json([], 204);
    }

    /**
     * Validate request.
     *
     * @param $request
     * @return array
     * @throws ValidationException
     */
    private function validateRequest($request) {
        return $this->validate($request, [
            'name' => 'required|string|max:255',
            'food_category_id' => 'required|exists:food_categories,id',
            'value' => 'required|numeric',
            'country' => 'required|string|max:255',
        ]);
    }
}
