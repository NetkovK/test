<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Density extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'food_category_id', 'value', 'country'
    ];

    public function foodCategory()
    {
        return $this->belongsTo(FoodCategory::class, 'food_category_id');
    }
}
