<?php

use App\Models\Density;

class DensityTest extends TestCase
{
    /** @test */
    public function testNameIsRequired()
    {
        $this->json('POST', '/', [])
            ->seeStatusCode(422)
            ->shouldReturnJson(["name" => ["The name field is required."]]);
    }

    /** @test */
    public function testNameLengthShouldBeLessThan255()
    {
        $this->json('POST', '/', ['name' => str_repeat("a", 256)])
            ->seeStatusCode(422)
            ->shouldReturnJson(["name" => ["The name may not be greater than 255 characters."]]);
    }

    /** @test */
    public function testCountryIsRequired()
    {
        $this->json('POST', '/', [])
            ->seeStatusCode(422)
            ->shouldReturnJson(["country" => ["The country field is required."]]);
    }

    /** @test */
    public function testCountryLengthShouldBeLessThan255()
    {
        $this->json('POST', '/', ['country' => str_repeat("a", 256)])
            ->seeStatusCode(422)
            ->shouldReturnJson(["country" => ["The country may not be greater than 255 characters."]]);
    }

    /** @test */
    public function testValueShouldBeNumber()
    {
        $this->json('POST', '/', ['value' => 'string'])
            ->seeStatusCode(422)
            ->shouldReturnJson(["value" => ["The value must be a number."]]);
    }

    /** @test */
    public function testFoodCategoryIdShouldBeExists()
    {
        $this->json('POST', '/', ['food_category_id' => 0])
            ->seeStatusCode(422)
            ->shouldReturnJson(["food_category_id" => ["The selected food category id is invalid."]]);
    }

    /** @test */
    public function testStore()
    {
        $this->json('POST', '/', factory(Density::class)->make()->toArray())
            ->seeStatusCode(201);
    }

    /** @test */
    public function testUpdate()
    {
        $density = factory(Density::class)->create();
        $data = $density->toArray();
        $data['name'] = 'New name';

        $this->json('PUT', '/0', $data)
            ->seeStatusCode(404);

        $this->json('PUT', '/' . $density->id, $data)
            ->seeStatusCode(204);
    }

    /** @test */
    public function testDelete()
    {
        $density = factory(Density::class)->create();

        $this->json('DELETE', '/0')
            ->seeStatusCode(404);

        $this->json('DELETE', '/' . $density->id)
            ->seeStatusCode(204);
    }
}
